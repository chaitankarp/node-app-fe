# NodeAppFe

Use the Angular CLI command to start the app.

`ng serve`

The project has a file env.js which contains the below properties: 

`(function (window) {
    window.__env = window.__env || {};
    window.__env.apiUrl = 'http://localhost:4000';
    window.__env.enableDebug = true;
  }(this));`


| Property | Description |
| ------ | ------ |
| apiUrl | The url pointing to the back-end API |
| enableDebug | Can be used to disable/enable debugging based on the environment | 

Note that this can be changed during runtime and also can deployed to different environments (staging, production) with different configurations without rebuilding the Angular application.

We can create users with 2 different roles.
*  ROLE_SUPERVISOR
*  ROLE_EMPLOYEE

While registering as an Employee, the user needs to mention his Supervisor username. The employee will have the ability to submit the task to his/her supervisor.
The supervisor can find the tasks of the employee's who are under his hierarchy and can Approvae/Reject the task submitted by them.